######################################################################
##
## STAGE 1: Build xtensa toolchain
##

FROM debian:stretch-slim
MAINTAINER https://gitlab.com/alelec/docker-esp-open-sdk

RUN sed -ri 's/^# deb /deb /g' /etc/apt/sources.list && \
    apt update; \
    apt install -y build-essential flex bison texinfo gawk \
	make unrar-free autoconf automake libtool-bin gcc g++ gperf \
    ncurses-dev libexpat-dev python-dev python python-serial \
    sed git unzip bash help2man wget bzip2

RUN useradd tools
RUN mkdir /tools && chown -R tools:tools /tools
USER tools
RUN git clone --recursive https://github.com/pfalcon/esp-open-sdk.git /tools
RUN cd /tools &&\
    echo "VERSION=$(git rev-parse --short HEAD)" &&\
    make

######################################################################
##
## STAGE 2: This is the final image.
##

FROM debian:stretch-slim

RUN sed -ri 's/^# deb /deb /g' /etc/apt/sources.list && \
    apt update; \
    apt install -y make autoconf automake libtool \
      python python-serial python3 python3-serial git zip && \
    rm -rf /var/cache/apk/*
	
COPY --from=0 /tools/xtensa-lx106-elf /tools/xtensa-lx106-elf
ENV PATH=/tools/xtensa-lx106-elf/bin:/bin:/usr/bin
RUN mkdir /usr/lib/esp-open-sdk
COPY install-wrapper /usr/bin/install-wrapper
COPY esp-open-sdk /usr/lib/esp-open-sdk/esp-open-sdk
